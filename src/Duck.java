import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

public class Duck implements Comparator<Duck>, Comparable<Duck>{
	private String name = "";
	public Duck(){
	}
	public Duck(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public String toString(){
		return name;
	}
	@Override
	public int compare(Duck o1, Duck o2) {
			return((Duck)o1).getName().compareTo(((Duck)o2).getName());
	}
	@Override
	public int compareTo(Duck o) {
			return this.getName().compareTo(((Duck) o).getName());
	}
	public static Optional<Duck> findDuck(ArrayList<Duck> temp, String n){
		for(Duck iter: temp)
			if(iter.getName().equalsIgnoreCase(n))
				return Optional.of(iter);
		return Optional.empty();
	}
}
