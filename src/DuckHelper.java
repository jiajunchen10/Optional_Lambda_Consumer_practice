import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;
public class DuckHelper{

	public static void main(String arg[]){
		Comparator<Duck> byName = (new Duck())::compare;
		ArrayList<Duck> myList= new ArrayList<>();
		
		Supplier<String> askInput = new Supplier<String>(){
			@Override
			public String get() {
				System.out.println("enter name of the duck: ");
				Scanner s = new Scanner(System.in);
				String result = s.nextLine();
				return result;
			}
		};
		myList.add(new Duck(askInput.get()));
		myList.add(new Duck("Benny"));
		myList.add(new Duck("Amy"));
		myList.add(new Duck("Tom"));
		myList.add(new Duck("Fred"));
		myList.add(new Duck("Alex"));
		
		System.out.println("\nprint list using method reference: ");
		myList.forEach(System.out::println);
		System.out.println("\nprint list using lambda expression: ");
		myList.forEach(temp->System.out.println(temp));
		System.out.println("\nprint list using Consumer interface ");
		myList.forEach(new Consumer<Duck>(){
			@Override
			public void accept(Duck t) {
				System.out.println(t.toString());
			}
		});
		System.out.println("finding a duck in the list, ");
		System.out.println(Duck.findDuck(myList, askInput.get()).isPresent()? "duck is in the list": "duck is not in the list");
	
		Stream<Duck> collection = myList.stream().distinct();
		collection.forEach(System.out::println);
		// Right System.out.println("any ducks name start with T?"+collection.anyMatch(x->x.getName().startsWith("T")));
		// Wrong System.out.println("any ducks name start with T?"+collection.findAny().ifPresent(System.out::println));
		System.out.println("any ducks name start with T? ");
		collection.filter(x->x.getName().startsWith("T")).forEach(System.out::print);
	}
}